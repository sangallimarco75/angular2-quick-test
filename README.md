# Dictionary Test

## Components communication
The comminication betweeen components is using an Observer Pattern design.
Components can talk using different channels called 
[Streams](https://bitbucket.org/sangallimarco75/angular2-quick-test/src/69ae978a9f240e219aad91a925ebb2b7cbab5d27/src/app/core/stream.service.ts?at=master&fileviewer=file-view-default#stream.service.ts-20)

Signals are filtered using the [component ID or (component ID + payload ID)](https://bitbucket.org/sangallimarco75/angular2-quick-test/src/69ae978a9f240e219aad91a925ebb2b7cbab5d27/src/app/core/stream.service.ts?at=master&fileviewer=file-view-default#stream.service.ts-25) in the case where component is reused multiple times.

Parent or Wrapper components can use the [dispatcher helper](https://bitbucket.org/sangallimarco75/angular2-quick-test/src/69ae978a9f240e219aad91a925ebb2b7cbab5d27/src/app/core/stream.service.ts?at=master&fileviewer=file-view-default#stream.service.ts-66) in order to configure the filters in a more organised way.


## Data Stores
This example is using [LocalStorage + Promise](https://bitbucket.org/sangallimarco75/angular2-quick-test/src/69ae978a9f240e219aad91a925ebb2b7cbab5d27/src/app/core/store.service.ts?at=master&fileviewer=file-view-default#store.service.ts-56) in order to simulate an API call. 

Fetch and Save methods can be easily replaced in order to use a real API layer.


## Installation
Please clone the repository and run the following commands

```
git clone git@bitbucket.org:sangallimarco75/angular2-quick-test.git
cd angular2-quick-test
npm install
```

### Run it
```
npm start
```
#### Testing
```
npm test
```