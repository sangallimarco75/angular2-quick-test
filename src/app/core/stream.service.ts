import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Injectable } from '@angular/core';
import { OnDestroy } from '@angular/core';
import 'rxjs/add/operator/filter';
import 'rxjs/Rx';

/**
 * Interface
 */
export interface StreamInterface {
    id: string,
    data: any
}

/**
 * Stream Class
 */
export class Stream extends Subject<StreamInterface>{

    /**
     * Filter by ids
     */
    filterById(id: string, dataId: string = undefined): Observable<StreamInterface> {
        return this.filter((data: StreamInterface) => dataId
            ? (data.id === id && data.data.id === dataId)
            : (data.id === id)
        );
    }

}

/**
 * Stream Component
 */
export class StreamComponent implements OnDestroy {
    protected streamManager: StreamService;

    constructor() {
        this.streamManager = new StreamService();
    }

    ngOnDestroy() {
        this.streamManager.removeSubscriptions();
    }
}

/**
 * Stream Utils
 * Use providers in component
 */
export class StreamService {
    private subscriptions: Array<Subscription> = [];

    /**
     * Run parallel tasks
     */
    forkJoin(streams: Array<Stream>): Observable<any> {
        return Observable.forkJoin(streams);
    }

    /**
     * use it on master components
     */
    dispatcher(stream: Stream, componentActions: any): Subscription {
        let subscription = stream
            .filter((x: StreamInterface) => componentActions.hasOwnProperty(x.id))
            .subscribe((x: StreamInterface) => componentActions[x.id](x));

        this.addSubscription(subscription);

        return subscription;
    }

    /**
     * Add a subscription
     * @param {Subscription} subscription [description]
     */
    addSubscription(subscription: Subscription): void {
        this.subscriptions.push(subscription);
    }

    /**
     * remove all subscriptions
     */
    removeSubscriptions(): void {
        this.subscriptions.forEach((subscription: Subscription) => {
            subscription.unsubscribe();
        });
    }
}

