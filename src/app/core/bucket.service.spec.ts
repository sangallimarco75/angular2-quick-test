import { BucketService, BucketDataStruct } from './bucket.service';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('BucketService', function () {
    let de: DebugElement;
    let service: BucketService = new BucketService();

    it('should check validity ', () => {
        let checkData: Array<BucketDataStruct> = [
            {
                uuid: '1',
                name: 'test'
            }
        ];

        let struct: BucketDataStruct = {
            uuid: '1',
            name: 'test'
        }

        // add item and force uuid
        let res = service.addItem(struct.name, struct.uuid);
        // get struct snapshot
        let data:Array<BucketDataStruct> = service.get();
        expect(checkData).toEqual(data);
    });
});
