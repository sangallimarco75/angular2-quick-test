import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';

export class BucketDataStruct {
    uuid: string;
    name: string;
}


@Injectable()
export class BucketService {
    // buckets ids
    private struct: Array<BucketDataStruct>;
    private buckets: string = 'buckets';

    constructor(
    ) {
        // auto-fetch all buckets
        this.fetch();
    }

    private check(name: string) {
        if (!name.length) {
            return false;
        }

        let found = this.struct.find((x: BucketDataStruct) => x.name === name);
        return found ? false : true;
    }

    private save(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let data: string = JSON.stringify(this.struct);
            localStorage.setItem(this.buckets, data);
            resolve(true);
        });
    }

    private add(data: BucketDataStruct): void {
        this.struct.push(data);
    }

    private remove(uuid: string): void {
        let indx = this.struct.findIndex((x: BucketDataStruct) => x.uuid === uuid);
        this.struct.splice(indx, 1);
    }

    get(): Array<BucketDataStruct> {
        return this.struct.map((x: BucketDataStruct) => Object.assign({}, x));
    }

    /**
     * Get All Buckets
     * @param bucketId 
     */
    fetch(): Promise<Array<BucketDataStruct>> {

        return new Promise((resolve, reject) => {
            let newStruct: Array<BucketDataStruct> = JSON.parse(localStorage.getItem(this.buckets));
            this.struct = newStruct || [];

            // immutable here?
            resolve(this.get());
        });

    }

    addItem(name: string, uuid: string = undefined): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // check for duplicates
            if (this.check(name)) {
                uuid = uuid || UUID.UUID();
                let data: BucketDataStruct = {
                    uuid,
                    name
                };
                this.add(data);
                this.save();
                resolve(true);
            } else {
                reject(false);
            }

        });
    }

    deleteItem(uuid: string): Promise<boolean> {
        this.remove(uuid);
        return this.save();
    }

}