import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';

/**
 * Struct
 */
export class DataStruct {
    uuid: string;
    domain: string;
    range: string;
}

export class DataValidationStruct {
    domain: boolean;
    range: boolean;
}

@Injectable()
export class StoreService {
    private struct: Array<DataStruct> = [];
    private bucketId: string = 'default';

    constructor(
    ) {
    }

    private check(flattenStruct: Array<string>, value: string): boolean {
        if (!value.length) {
            return false;
        }
        let found: string = flattenStruct.find((x: string) => x === value);
        return found ? false : true;
    }

    /**
     * return flatten struct but selected uuid
     */
    private flatten(uuid: string): Array<string> {
        let flattenStruct: Array<string> = [];

        this.struct
            .filter(
            (x: DataStruct) => {
                return x.uuid !== uuid;
            }
            )
            .forEach(
            (x: DataStruct) => {
                flattenStruct.push(...[x.domain, x.range]);
            }
            );

        return flattenStruct;
    }

    private isNew(uuid: string) {
        let found = this.struct.find((x: DataStruct) => x.uuid === uuid);
        return found ? false : true;
    }

    private save(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let data: string = JSON.stringify(this.struct);
            localStorage.setItem(this.bucketId, data);
            resolve(true);
        });
    }


    private add(data: DataStruct): void {
        this.struct.push(data);
    }

    private remove(uuid: string): void {
        let indx = this.struct.findIndex((x: DataStruct) => x.uuid === uuid);
        this.struct.splice(indx, 1);
    }

    private update(data: DataStruct): void {
        let item: DataStruct = this.struct.find((x: DataStruct) => x.uuid === data.uuid);

        // change values
        if (item) {
            Object.assign(item, data);
        }
    }

    // public ------------------------------------------------------

    get(): Array<DataStruct> {
        return this.struct.map((x: DataStruct) => Object.assign({}, x));
    }

    fetch(bucketId: string): Promise<Array<DataStruct>> {
        this.bucketId = bucketId;

        return new Promise((resolve, reject) => {
            let newStruct: Array<DataStruct> = JSON.parse(localStorage.getItem(this.bucketId));
            this.struct = newStruct || [];

            // immutable here?
            resolve(this.get());
        });

    }

    // public
    updateItem(uuid: string, domain: string, range: string): Promise<DataValidationStruct> {
        return new Promise((resolve, reject) => {

            // create new UUID if not detected
            uuid = uuid || UUID.UUID();
            let isNew = this.isNew(uuid);
            
            // create flatten struct
            let flatten: Array<string> = this.flatten(uuid);

            let domainCheck = this.check(flatten, domain);
            let rangeCheck = this.check(flatten, range)  && domain !== range;

            let validationResult: DataValidationStruct = {
                domain: domainCheck,
                range: rangeCheck
            };


            // check if checks are passing
            if (domainCheck && rangeCheck) {

                // create struct
                let data: DataStruct = {
                    uuid,
                    domain,
                    range
                };

                // if new 
                if (isNew) {
                    this.add(data);
                } else {
                    this.update(data);
                }

                // save data
                this.save();

                resolve(validationResult);
            } else {
                reject(validationResult);
            }
        });
    }

    deleteItem(uuid: string): Promise<boolean> {
        this.remove(uuid);
        return this.save();
    }


}