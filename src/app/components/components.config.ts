import { Stream } from '../core/stream.service';

export const STREAMS = {
    CHANGE: new Stream(),
    INJECT: new Stream()
};

export const COMPONENTS = {
    ITEM: 'ITEM',
    LIST: 'LIST',
    BUCKET: 'BUCKET',
    BUCKET_ITEM: 'BUCKET_ITEM',
    BUCKETS_LIST: 'BUCKETS_LIST'
};

export const INTENTS = {
    UPDATE: 'UPDATE',
    DELETE: 'DELETE',
    LINK: 'LINK'
};