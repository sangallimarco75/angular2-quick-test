import { Component, OnInit, Input } from '@angular/core';
import { BucketDataStruct } from '../core/bucket.service';
import { StreamInterface, StreamComponent } from '../core/stream.service';
import { COMPONENTS, STREAMS, INTENTS } from './components.config';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'bucket-item-component',
    template: `<div class="row">
                    <div class="row-item">{{item.name}}</div>
                    <div class="row-item">
                        <button class="btn btn-primary" (click)="handleLink()">Dictionary</button>
                        <button class="btn btn-danger" (click)="handleDelete()">Delete</button>
                    </div>
                </div>
    `,
    providers: [],
    styleUrls: ['./list.component.css']
})
export class BucketItemComponent extends StreamComponent implements OnInit {

    @Input() item: BucketDataStruct;
    public state: boolean = true;

    constructor(
    ) {
        super();
    }

    ngOnInit(): void {
    }

    handleLink(): void {
        this.propagateChange(INTENTS.LINK);
    }

    handleDelete(): void {
        this.propagateChange(INTENTS.DELETE);
    }

    propagateChange(intent: string): void {
        let payload: StreamInterface = {
            id: COMPONENTS.BUCKET_ITEM,
            data: {
                id: this.item.uuid,
                item: this.item,
                intent
            }
        }
        STREAMS.CHANGE.next(payload);
    }
}
