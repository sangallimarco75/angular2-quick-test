import { Component, OnInit } from '@angular/core';
import { BucketService, BucketDataStruct } from '../core/bucket.service';
import { StreamInterface, StreamComponent } from '../core/stream.service';
import { COMPONENTS, STREAMS, INTENTS } from './components.config';
import { Router } from '@angular/router';

@Component({
    selector: 'buckets-component',
    template: `<h1>Buckets List</h1>

                <div class="row">
                    <div class="row-item title">Name</div>
                    <div class="row-item title">Actions</div>
                </div>

                <bucket-component [item]="item"></bucket-component>

                <bucket-item-component *ngFor="let item of buckets" [item]="item"></bucket-item-component>

                <h2>Debug</h2>
                <pre>{{buckets | json}}</pre>
                <div></div>`,
    providers: [BucketService],
    styleUrls: ['./list.component.css']
})
export class BucketsComponent extends StreamComponent implements OnInit {

    public buckets: Array<BucketDataStruct> = [];

    private newItemId: string = '***';
    public item: BucketDataStruct = {
        uuid: this.newItemId,
        name: undefined
    };

    constructor(
        private router: Router,
        private BucketService: BucketService,
    ) {
        super();
    }

    private loadData(): void {
        this.BucketService.fetch()
            .then(
            (buckets: Array<BucketDataStruct>) => {
                this.buckets = buckets;
            }
            );
    }

    private notifyItem(id: string, status: boolean): void {
        let payload: StreamInterface = {
            id: COMPONENTS.BUCKET,
            data: {
                id,
                status
            }
        }
        STREAMS.INJECT.next(payload);
    }

    ngOnInit(): void {

        const bucketItemActions = {
            [INTENTS.LINK]: (item: BucketDataStruct) => {
                console.log('link');
                this.router.navigate(['/list', item.name]);
            },
            [INTENTS.DELETE]: (item: BucketDataStruct) => {
                this.BucketService.deleteItem(item.uuid)
                    .then(
                    res => {
                        this.loadData();
                    }
                    );
            }
        };

        // actions from components
        const actions = {
            [COMPONENTS.BUCKET]: (res: StreamInterface) => {
                let item: BucketDataStruct = res.data.item;

                this.BucketService.addItem(item.name)
                    .then(
                    res => {
                        this.notifyItem(this.newItemId, true);
                        this.loadData();
                    },
                    err => this.notifyItem(this.newItemId, false)
                    );
            },
            [COMPONENTS.BUCKET_ITEM]: (res: StreamInterface) => {
                let item: BucketDataStruct = res.data.item;
                bucketItemActions[res.data.intent](item);
            }
        };

        this.streamManager.dispatcher(STREAMS.CHANGE, actions);

        this.loadData();
    }
}
