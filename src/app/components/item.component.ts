import { Component, OnInit, Input } from '@angular/core';
import { DataStruct, DataValidationStruct } from '../core/store.service';
import { StreamInterface, StreamComponent } from '../core/stream.service';
import { COMPONENTS, STREAMS, INTENTS } from './components.config';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'item-component',
    template: `<div class="row" [ngClass]="{'row-error': !status}">
                    <div class="row-item"><input [ngClass]="{'row-item-error': !validation.domain}" [(ngModel)]="itemModel.domain" placeholder="Enter Domain" type="text" /></div>
                    <div class="row-item"><input [ngClass]="{'row-item-error': !validation.range}" [(ngModel)]="itemModel.range" placeholder="Enter Range" type="text" /></div>
                    <div class="row-item">
                        <button class="btn btn-primary" (click)="handleSave()">{{edit ? 'Save' : 'Create New'}}</button>
                        <button *ngIf="edit" class="btn btn-danger" (click)="handleDelete()">Delete</button>
                    </div>
                </div>
    `,
    providers: [],
    styleUrls: ['./list.component.css']
})
export class ItemComponent extends StreamComponent implements OnInit {

    @Input() item: DataStruct;
    @Input() edit: boolean;
    public itemModel: DataStruct;
    public status: boolean;
    public validation: DataValidationStruct;

    constructor(
    ) {
        super();
    }

    ngOnInit(): void {
        this.initModel();

        // events coming from parent
        let subscription: Subscription = STREAMS.INJECT
            .filterById(COMPONENTS.ITEM, this.item.uuid)
            .subscribe(
            (res: StreamInterface) => {
                // set state if parent wants to display an error
                this.validation = res.data.validation;
                this.status = res.data.status;

                // clear if all is good
                if (this.status && !this.edit) {
                    this.initModel();
                }
            }
            );

        this.streamManager.addSubscription(subscription);

        // clone model
        this.itemModel = Object.assign({}, this.item);
    }

    handleSave(): void {
        this.propagateChange(INTENTS.UPDATE);
    }

    handleDelete(): void {
        this.propagateChange(INTENTS.DELETE);
    }

    initModel(): void {
        // clean model
        this.itemModel = {
            uuid: this.item.uuid,
            domain: undefined,
            range: undefined
        };

        this.validation = {
            domain: true,
            range: true
        };

        this.status = true;
    }

    propagateChange(intent: string): void {
        let payload: StreamInterface = {
            id: COMPONENTS.ITEM,
            data: {
                id: this.edit ? this.item.uuid : undefined,
                item: this.itemModel,
                intent
            }
        }
        STREAMS.CHANGE.next(payload);
    }
}
