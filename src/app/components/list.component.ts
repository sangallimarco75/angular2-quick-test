import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { DataStruct, StoreService, DataValidationStruct } from '../core/store.service';
import { StreamInterface, StreamComponent } from '../core/stream.service';
import { COMPONENTS, STREAMS, INTENTS } from './components.config';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'list-component',
    template: `<h1>Dictionary For Bucket [{{bucketId}}]</h1>

                <div class="row">
                    <div class="row-item title">Domain</div>
                    <div class="row-item title">Range</div>
                    <div class="row-item title">Actions</div>
                </div>

                <item-component [item]="item" [edit]="false"></item-component>

                <item-component *ngFor="let item of dictionary" [item]="item" [edit]="true"></item-component>

                <h2>Debug</h2>
                <pre>{{dictionary | json}}</pre>
                <div></div>`,
    providers: [StoreService],
    styleUrls: ['./list.component.css']
})
export class ListComponent extends StreamComponent implements OnInit {

    public dictionary: Array<DataStruct> = [];
    private actions: Object = {};
    public bucketId: string;
    private newItemId: string = '***';

    public item: DataStruct = {
        uuid: this.newItemId,
        domain: undefined,
        range: undefined
    };

    constructor(
        private StoreService: StoreService,
        private route: ActivatedRoute,
        private location: Location
    ) {
        super();
    }

    private notifyItem(id: string, validation: DataValidationStruct, status: boolean): void {
        let payload: StreamInterface = {
            id: COMPONENTS.ITEM,
            data: {
                id,
                validation,
                status
            }
        }
        STREAMS.INJECT.next(payload);
    }

    private setBucketId(id: string): void {
        this.bucketId = id;
        this.loadData();
    }

    private loadData(): void {
        this.StoreService.fetch(this.bucketId)
            .then(
            (dictionary: Array<DataStruct>) => this.dictionary = dictionary
            );
    }

    ngOnInit(): void {

        const itemActions = {
            [INTENTS.UPDATE]: (item: DataStruct) => {
                let uuid = item.uuid === this.newItemId ? null : item.uuid;

                this.StoreService.updateItem(uuid, item.domain, item.range)
                    .then(
                    (res: DataValidationStruct) => {
                        this.notifyItem(item.uuid, res, true);
                        this.loadData();
                    },
                    (res: DataValidationStruct) => this.notifyItem(item.uuid, res, false)
                    );
            },
            [INTENTS.DELETE]: (item: DataStruct) => {
                this.StoreService.deleteItem(item.uuid)
                    .then(
                    res => {
                        this.loadData();
                    }
                    );
            }
        }

        // actions from components
        const actions = {
            [COMPONENTS.ITEM]: (res: StreamInterface) => {
                let item: DataStruct = res.data.item;
                itemActions[res.data.intent](item);
            }
        };

        this.streamManager.dispatcher(STREAMS.CHANGE, actions);

        this.route.params
            .subscribe(
            (params: Params) => this.setBucketId(params['id'])
            );
    }
}
