import { Component, OnInit, Input } from '@angular/core';
import { BucketDataStruct } from '../core/bucket.service';
import { StreamInterface, StreamComponent } from '../core/stream.service';
import { COMPONENTS, STREAMS, INTENTS } from './components.config';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'bucket-component',
    template: `<div class="row" [ngClass]="{'row-error': !status}">
                    <div class="row-item"><input [ngClass]="{'row-item-error': !status}"  [(ngModel)]="itemModel.name" placeholder="Enter Name" type="text" /></div>
                    <div class="row-item">
                        <button class="btn btn-primary" (click)="handleSave()">Create New</button>
                    </div>
                </div>
    `,
    providers: [],
    styleUrls: ['./list.component.css']
})
export class BucketComponent extends StreamComponent implements OnInit {

    @Input() item: BucketDataStruct;
    public itemModel: BucketDataStruct;
    public status: boolean = true;

    constructor(
    ) {
        super();
    }

    ngOnInit(): void {
        // clone model
        this.itemModel = Object.assign({}, this.item);

        // events coming from parent
        let subscription: Subscription = STREAMS.INJECT
            .filterById(COMPONENTS.BUCKET, this.item.uuid)
            .subscribe(
            (res: StreamInterface) => {
                console.log('incoming', res);
                // set status if parent wants to display an error
                this.status = res.data.status;

                // clear if all is good
                if (this.status) {
                    this.initModel();
                }
            }
            );

        this.streamManager.addSubscription(subscription);
    }

    handleSave(): void {
        this.propagateChange(INTENTS.UPDATE);
    }

    handleDelete(): void {
        this.propagateChange(INTENTS.DELETE);
    }

    initModel(): void {
        // clean model
        this.itemModel = {
            uuid: undefined,
            name: undefined
        };
    }

    propagateChange(intent: string): void {
        let payload: StreamInterface = {
            id: COMPONENTS.BUCKET,
            data: {
                id: this.item.uuid,
                item: this.itemModel,
                intent
            }
        }
        STREAMS.CHANGE.next(payload);
    }
}
