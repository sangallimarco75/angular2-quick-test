import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
            <div class="app">
                <h1>Dictionary Consistency SPA</h1>
                <nav>
                  <a routerLink="/">Home</a>
                  <a routerLink="/list">Buckets</a>
                </nav>
                <router-outlet></router-outlet>
             </div>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = 'Angular';
}
