import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ListComponent } from './components/list.component';
import { ItemComponent } from './components/item.component';
import { BucketsComponent } from './components/buckets.component';
import { BucketComponent } from './components/bucket.component';
import { BucketItemComponent } from './components/bucket-item.component';
import { HomeComponent } from './components/home.component';

const routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'list',
    component: BucketsComponent
  },
  {
    path: 'list/:id',
    component: ListComponent
  }
];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [
    AppComponent,
    BucketsComponent,
    ListComponent,
    ItemComponent,
    BucketComponent,
    BucketItemComponent,
    HomeComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



